//
//  Product.swift
//  SellProduct
//
//  Created by Anh Tuan on 4/2/18.
//  Copyright © 2018 Anh Tuan. All rights reserved.
//

import UIKit

class Product: NSObject {
    var uuid: String
    var fullName: String
    var address : String
    var productName : String
    var count: Int
    var imageUrl: String
    init(fullName: String, address : String, productName: String, count: Int, imageUrl: String, uuid: String) {
        self.fullName = fullName
        self.address = address
        self.productName = productName
        self.count = count
        self.imageUrl = imageUrl
        self.uuid = uuid
    }
    
    init(fullName: String, address : String, productName: String, count: Int) {
        self.fullName = fullName
        self.address = address
        self.productName = productName
        self.count = count
        self.imageUrl = ""
        self.uuid = ""
    }
    init(item : NSDictionary, uuid: String) {
        self.uuid = uuid
        self.fullName = item.value(forKey: "fullName") as! String
        self.count = item.value(forKey: "count") as! Int
        self.address = item.value(forKey: "address") as! String
        self.productName = item.value(forKey: "productName") as! String
        self.imageUrl = item.value(forKey: "imgUrl") as! String
    }
    
    func toDictionary() -> [String: Any] {
        return [
            "fullName": self.fullName,
            "count": self.count,
            "imgUrl": self.imageUrl,
            "productName": self.productName,
            "address": self.address
        ]
    }
}
