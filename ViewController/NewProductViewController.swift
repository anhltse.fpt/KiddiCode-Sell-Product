//
//  NewProductViewController.swift
//  SellProduct
//
//  Created by Anh Tuan on 4/2/18.
//  Copyright © 2018 Anh Tuan. All rights reserved.
//

import UIKit

protocol delegateProduct {
    func addProduct(product: Product)
    func deleteProduct(product: Product)
}

class NewProductViewController: BaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var tfFullName : UITextField!
    @IBOutlet weak var tfAddress : UITextField!
    @IBOutlet weak var tfProduct : UITextField!
    @IBOutlet weak var tfCount : UITextField!
    
    @IBOutlet weak var btnPost : UIButton!
    @IBOutlet weak var img : UIImageView!
    
    var imagePicker = UIImagePickerController()
    var delegate : delegateProduct?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        self.title = "Sell"
        self.view.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard)))
        self.btnPost.isEnabled = false
        // Do any additional setup after loading the view.
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func post () {
        let fullName = tfFullName.text
        let address = tfAddress.text
        let product = tfProduct.text
        let count = Int(tfCount.text!)
        
        if ((fullName == nil) || (address == nil) || (product == nil) || (count == nil)) {
            self.btnPost.isEnabled = false
            return
        }
        if (self.img.image == nil) {
            self.btnPost.isEnabled = false
            return
        }
        self.showLoadingHUD()
        let newProduct = Product.init(fullName: fullName!, address: address!, productName: product!, count: count!)
        APIManager.sharedInstance.addProduct(product: newProduct, image: self.img.image!) { (product) in
            if (self.delegate != nil) {
                self.delegate?.addProduct(product: product)
            }
            self.hideLoadingHUD()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func takePicture() {
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func openGallery() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            img.image = image
        } else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            img.image = image
        } else {
            img.image = nil
        }
        self.checkBtnEnable()
    }
    
    func checkBtnEnable() {
        let fullName = tfFullName.text
        let address = tfAddress.text
        let product = tfProduct.text
        let count = Int(tfCount.text!)
        
        if ((fullName == nil) || (address == nil) || (product == nil) || (count == nil)) {
            self.btnPost.isEnabled = false
            return
        }
        if (self.img.image == nil) {
            self.btnPost.isEnabled = false
            return
        }
        self.btnPost.isEnabled = true
    }
    
    @IBAction func editChange(_ sender: Any) {
        self.checkBtnEnable()
    }
}

