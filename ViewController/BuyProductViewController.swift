//
//  SellProductViewController.swift
//  SellProduct
//
//  Created by Anh Tuan on 4/3/18.
//  Copyright © 2018 Anh Tuan. All rights reserved.
//

import UIKit

class BuyProductViewController: BaseViewController {
    var delegate: delegateProduct?
    @IBOutlet weak var tfFullName : UITextField!
    @IBOutlet weak var tfEmail : UITextField!
    
    @IBOutlet weak var btnPost : UIButton!
    var product: Product?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Buy"
        self.btnPost.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func post () {
        let userName = tfFullName.text
        let email = tfEmail.text
        if ((userName == nil) || (email == nil)) {
            self.btnPost.isEnabled = false
            return
        }
        self.showLoadingHUD()
        self.btnPost.isEnabled = true
        APIManager.sharedInstance.sellProduct(uuid: self.product!.uuid, product: self.product!, userName: userName!, email: email!) {
            self.hideLoadingHUD()
            if (self.delegate != nil) {
                self.delegate?.deleteProduct(product: self.product!)
            }
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func checkBtnEnable() {
        let userName = tfFullName.text
        let email = tfEmail.text
        if ((userName == nil) || (email == nil)) {
            self.btnPost.isEnabled = false
            return
        }
        self.btnPost.isEnabled = true
    }
    
    @IBAction func editChange(_ sender: Any) {
        self.checkBtnEnable()
    }
}
