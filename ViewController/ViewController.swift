//
//  ViewController.swift
//  SellProduct
//
//  Created by Anh Tuan on 4/2/18.
//  Copyright © 2018 Anh Tuan. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {
    var listProduct = [Product]()
    @IBOutlet weak var tbl: UITableView!
    let identifierCell = "ProductTableViewCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Kiddi Code"
        self.setupTableView()
        self.setupRightButton()
        self.showLoadingHUD()
        tbl.tableFooterView = UIView.init(frame: CGRect.zero)
        APIManager.sharedInstance.getListProduct { (listProduct) in
            self.listProduct = listProduct
            DispatchQueue.main.async {
                self.hideLoadingHUD()
                self.tbl.reloadData()
            }
        }
    }
    
    func setupRightButton() {
        let anotherButton = UIBarButtonItem.init(title: "Add", style: .plain, target: self, action: #selector(onAddTap))
        self.navigationItem.rightBarButtonItem = anotherButton
    }
    
    @objc func onAddTap () {
        let newVC = self.storyboard?.instantiateViewController(withIdentifier: "NewProductViewController") as! NewProductViewController
        newVC.delegate = self
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func setupTableView() {
        tbl.register(UINib.init(nibName: identifierCell, bundle: nil), forCellReuseIdentifier: identifierCell)
        
        tbl.rowHeight = UITableViewAutomaticDimension
        tbl.estimatedRowHeight = 100
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
extension ViewController : UITableViewDataSource, UITableViewDelegate, delegateProduct {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listProduct.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifierCell) as! ProductTableViewCell
        cell.setData(product: self.listProduct[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let sellVC = self.storyboard?.instantiateViewController(withIdentifier: "BuyProductViewController") as! BuyProductViewController
        sellVC.delegate = self
        sellVC.product = self.listProduct[indexPath.row]
        self.navigationController?.pushViewController(sellVC, animated: true)
    }
    func addProduct(product: Product) {
        self.listProduct.append(product)
        DispatchQueue.main.async {
            self.tbl.reloadData()
        }
    }
    func deleteProduct(product: Product) {
        self.listProduct = self.listProduct.filter { (item) -> Bool in
            item.uuid != product.uuid
        }
        DispatchQueue.main.async {
            self.tbl.reloadData()
        }
    }
}
