//
//  APIManager.swift
//  SellProduct
//
//  Created by Anh Tuan on 4/2/18.
//  Copyright © 2018 Anh Tuan. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import FirebaseDatabase

class APIManager: NSObject {
    static let sharedInstance = APIManager()
    var ref: DatabaseReference!
    override init() {
        ref = Database.database().reference()
    }
    func getListProduct (complete: @escaping (([Product]) -> Void)) {
        var result = [Product]()
        self.ref.child("product").observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? NSDictionary
            if ((value) != nil) {
                for (key,item) in value! {
                    result.append(Product.init(item: item as! NSDictionary, uuid: key as! String))
                }
            }
            complete(result)
        }
        
    }
    
    func addProduct (product: Product, image: UIImage, complete: @escaping ((Product) -> Void)) {
        let uuid = UUID().uuidString
        uploadImageToFireBase(image: image, uuid: uuid) { (url) in
            let productAdd = [
                "fullName": product.fullName,
                "address": product.address,
                "productName": product.productName,
                "count": product.count,
                "imgUrl": url
                ] as [String : Any]
            self.ref.child("product/\(uuid)").setValue(productAdd)
            let newProduct = Product.init(fullName: product.fullName, address: product.address, productName: product.productName, count: product.count, imageUrl: url, uuid: uuid)
            complete(newProduct)
        }
    }
    
    func uploadImageToFireBase (image: UIImage, uuid: String,complete: @escaping ((String) -> Void)) {
        let imageUpload = image.resizeImage(targetSize: CGSize.init(width: 500, height: 500))
        let storage = Storage.storage()
        var data = Data()
        data = UIImagePNGRepresentation( imageUpload)! // image file name
        // Create a storage reference from our storage service
        let storageRef = storage.reference()
        var imageRef = storageRef.child("images/\(uuid).png")
        _ = imageRef.putData(data, metadata: nil, completion: { (metadata,error ) in
            guard let metadata = metadata else{
                print("Error:  \(error!)")
                complete("")
                return
            }
            let downloadURL = metadata.downloadURL()
            let url = downloadURL?.absoluteString
            complete(url!)
        })
    }
    
    func sellProduct (uuid: String, product: Product, userName: String, email : String, complete: @escaping(() -> Void)) {
        self.ref.child("product/\(uuid)").setValue(nil)
        let value = [
            "product": product.toDictionary(),
            "username": userName,
            "email": email
            ] as [String : Any]
        self.ref.child("listSell/\(uuid)").setValue(value)
        complete()
    }
}
