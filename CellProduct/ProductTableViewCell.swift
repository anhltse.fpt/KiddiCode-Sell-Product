//
//  ProductTableViewCell.swift
//  SellProduct
//
//  Created by Anh Tuan on 4/2/18.
//  Copyright © 2018 Anh Tuan. All rights reserved.
//

import UIKit
import SDWebImage

class ProductTableViewCell: UITableViewCell {
    @IBOutlet weak var img : UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblContent : UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(product: Product) {
        self.img.sd_setImage(with: URL.init(string: product.imageUrl), placeholderImage: UIImage.init(named: "imagePlaceholder"), options: .continueInBackground, completed: nil)
        self.lblName.text = product.fullName
        let content = "- \(product.productName): \(product.count)"
        self.lblContent.text = content
        let address = "- Địa chỉ: \(product.address)"
        self.lblAddress.text = address
    }
    
}
